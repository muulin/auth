package input

import "errors"

type V3_Login struct {
	Account  string `json:"account"`
	Pwd      string `json:"pwd"`
	Push     string `json:"push"`
	Platform string `json:"platform"`
	PhoneId  string `json:"phoneID"`
}

func (in *V3_Login) Validate() error {
	if in.Account == "" {
		return errors.New("missing account")
	}
	if in.Pwd == "" {
		return errors.New("missing pwd")
	}
	if in.Push == "" {
		return errors.New("missing push")
	}
	if in.Platform == "" {
		return errors.New("missing platform")
	}
	if in.PhoneId == "" {
		return errors.New("missing phoneID")
	}
	return nil
}
