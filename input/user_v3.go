package input

import "errors"

type CreateUserProjectFolders []*struct {
	Name        string
	ProjectList []string `json:"projectList"`
}

func (in CreateUserProjectFolders) Validate() error {
	for _, v := range in {
		if v.Name == "" {
			return errors.New("missing name")
		}
	}
	return nil
}
