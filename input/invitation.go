package input

import (
	"errors"
	"time"

	dao "auth/dao"
	types "auth/types"

	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/util"
)

type CreateMail struct {
	Type    types.RegisterType
	Name    string
	Email   string
	Channel string
	Roles   []auth.UserPerm //admin
}

func (c *CreateMail) Validate() error {
	if !c.Type.IsValid() {
		return errors.New("invalid type")
	}

	valid, _ := util.IsMail(c.Email)
	if !valid {
		return errors.New("invalid email")
	}

	for _, r := range c.Roles {
		if !r.Validate() {
			return errors.New("invalid roles")
		}
	}

	if c.Channel == "" {
		return errors.New("channel cant be empty")
	}

	return nil
}

func (m *CreateMail) ToNewDao() *dao.Invitation {
	d := &dao.Invitation{
		Name:      m.Name,
		Email:     m.Email,
		Channel:   m.Channel,
		State:     types.InvitationStateNew,
		Type:      m.Type,
		Roles:     m.Roles,
		CreatedAt: time.Now(),
	}

	return d
}

type ConfirmInvite struct {
	State types.InvitationState
}
