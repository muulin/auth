package input

import (
	"errors"
	"os"

	dao "auth/dao"

	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/util"
)

const (
	emptyStr = ""
)

type AdminRegister struct {
	Code     string
	Email    string
	Pwd      string
	Name     string
	Lang     string
	Platform string
	Roles    []auth.UserPerm
	Channels []string
}

func (qb *AdminRegister) Validate() error {
	if qb.Code == emptyStr {
		return errors.New("missing code")
	}
	if qb.Email == emptyStr {
		return errors.New("missing email")
	}
	if qb.Pwd == emptyStr {
		return errors.New("missing pwd")
	}
	if qb.Name == emptyStr {
		return errors.New("missing Name")
	}
	for _, p := range qb.Roles {
		if !p.Validate() {
			return errors.New("invalid user perm")
		}
	}
	if qb.Code != os.Getenv("Auth_Code") {
		return errors.New("invalid code")
	}
	return nil
}

type Login struct {
	Account, Pwd string
}

func (qb *Login) Validate() error {
	if qb.Account == emptyStr {
		return errors.New("missing account")
	}
	if qb.Pwd == emptyStr {
		return errors.New("missing pwd")
	}
	return nil
}

type Register struct {
	InvitationID string `json:"invitationId" mapstructure:"password"`
	Pwd          string `json:"pwd" mapstructure:"password"`
	Name         string
	Lang         string
	Meta         map[string]interface{}
}

func (qb *Register) ToDaoRegister() *dao.Register {
	return &dao.Register{
		InvitationID: qb.InvitationID,
		Name:         qb.Name,
		Pwd:          qb.Pwd,
		Lang:         qb.Lang,
		Meta:         qb.Meta,
	}
}

func (qb *Register) Validate() error {
	if qb.InvitationID == emptyStr {
		return errors.New("invalid invitationID")
	}
	if qb.Pwd == emptyStr {
		return errors.New("miss pwd")
	}
	if qb.Name == emptyStr {
		return errors.New("miss name")
	}
	if qb.Lang == emptyStr {
		return errors.New("miss lang")
	}
	return nil
}

type SetMailTemplate struct {
	Title      string
	Html       string
	Txt        string
	Paramaters []*dao.Paramater
}

func (c *SetMailTemplate) ToDaoMailTemplate() *dao.MailTemplate {
	return &dao.MailTemplate{
		Title:     c.Title,
		Html:      c.Html,
		Txt:       c.Txt,
		Parmaters: c.Paramaters,
	}
}

func (c *SetMailTemplate) Validate() error {
	if c.Title == emptyStr {
		return errors.New("missing title")
	}
	if c.Html == emptyStr {
		return errors.New("missing html")
	}
	if c.Txt == emptyStr {
		return errors.New("missing txt")
	}
	return nil
}

type Mail struct {
	Email string
}

func (c *Mail) Validate() error {
	if c.Email == emptyStr {
		return errors.New("missing mail")
	}
	if ok, _ := util.IsMail(c.Email); !ok {
		return errors.New("invalid email format")
	}
	return nil
}

type ChangePwd struct {
	NewPwd string `json:"new-pwd"`
	OldPwd string `json:"pwd"`
}

func (c *ChangePwd) Validate() error {
	if c.NewPwd == emptyStr {
		return errors.New("missing new-pwd")
	}
	if c.OldPwd == emptyStr {
		return errors.New("missing pwd")
	}
	return nil
}

type AddUserPlatform struct {
	Account  string
	Platform string
	Roles    []auth.UserPerm
}

func (c *AddUserPlatform) Validate() error {
	if c.Account == emptyStr {
		return errors.New("missing account")
	}
	for _, role := range c.Roles {
		if !role.Validate() {
			return errors.New("invalid role: " + string(role))
		}
	}
	return nil
}
