package mygrpc

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"auth/dao"

	"auth/input"
	"auth/model"
	"auth/types"

	"bitbucket.org/muulin/interlib/auth/pb"
	"bitbucket.org/muulin/interlib/diutil"
	"github.com/94peter/sterna"
	"github.com/94peter/sterna/auth"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type serviceDI interface {
	db.RedisDI
	sterna.CommonDI
}

func getServiceDI(ctx context.Context) serviceDI {
	val := sterna.GetDiByCtx(ctx)
	if di, ok := val.(serviceDI); ok {
		return di
	}
	return nil
}

func NewService() pb.AuthServiceServer {
	return &server{}
}

type server struct {
	servName string
	pb.UnimplementedAuthServiceServer
}

// 更新裝置上傳數據
func (s *server) GetUserInfo(ctx context.Context, request *pb.GetUserInfoRequest) (*pb.GetUserInfoResponse, error) {
	di := getServiceDI(ctx)
	if di == nil {
		return nil, status.Error(codes.Internal, "di was not found")
	}
	jwtDI := di.(auth.JwtDI)
	userToken := model.NewJwtUserToken(jwtDI.NewJwt())
	result, err := userToken.Parser(request.Token)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "token in invalid")
	}
	if os.Getenv("environment") != "dev" {
		if result.Host() != request.Host {
			return &pb.GetUserInfoResponse{
				StatusCode: http.StatusForbidden,
				Message:    "host not match",
			}, nil
		}
	}

	return &pb.GetUserInfoResponse{
		StatusCode: http.StatusOK,
		Sub:        result.Sub(),
		Account:    result.Account(),
		Name:       result.Name(),
		Roles:      result.Perms(),
		Channels:   result.Channels(),
	}, nil
}

func (s *server) GetAccount(ctx context.Context, req *pb.GetAccountRequest) (*pb.GetAccountResponse, error) {
	dbclt := db.GetMgoDBClientByCtx(ctx)
	l := log.GetLogByCtx(ctx)
	crud := model.NewCRUD(ctx, dbclt.GetCoreDB(), l)
	oid, err := sternaDao.GetObjectID(req.Id)
	if err != nil {
		return nil, status.Error(codes.NotFound, "invalid id: "+req.Id)
	}
	u, err := crud.FindByID(oid)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return &pb.GetAccountResponse{
		Account: u.Email,
	}, nil
}

func (s *server) IsAccountExist(ctx context.Context, req *pb.IsAccountExistRequest) (*pb.IsAccountExistResponse, error) {
	dbclt := db.GetMgoDBClientByCtx(ctx)
	l := log.GetLogByCtx(ctx)
	crud := model.NewCRUD(ctx, dbclt.GetCoreDB(), l)
	result, err := crud.AccountExists([]string{req.Account})
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.IsAccountExistResponse{
		Exists: result[req.Account],
	}, nil
}

func (s *server) CreateInvitation(ctx context.Context, req *pb.CreateInvitationRequest) (*pb.CreateInvitationResponse, error) {

	dbclt := db.GetMgoDBClientByCtx(ctx)
	l := log.GetLogByCtx(ctx)

	crud := model.NewCRUD(ctx, dbclt.GetCoreDB(), l)
	result, err := crud.AccountExists([]string{req.Email})
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if result["email"] {
		return nil, status.Error(codes.AlreadyExists, "email exists")
	}

	intivationCrud := model.NewIntivation(ctx, dbclt.GetCoreDB(), l)

	invitation, err := intivationCrud.FindNewByEmail(req.Email)
	if err != nil {
		fmt.Println("invitation not found", err)
		return nil, status.Error(codes.Internal, err.Error())
	}
	if invitation != nil {
		return &pb.CreateInvitationResponse{
			Id: invitation.ID.Hex(),
		}, nil
	}

	id, err := intivationCrud.Create(&input.CreateMail{
		Type:    types.UserRegister,
		Name:    req.Name,
		Email:   req.Email,
		Channel: req.Channel,
		Roles:   []auth.UserPerm{auth.PermMember},
	})
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.CreateInvitationResponse{
		Id: id.Hex(),
	}, nil
}

func (s *server) ForgetPassword(ctx context.Context, req *pb.ForgetPasswordRequest) (*pb.ForgetPasswordResponse, error) {
	di := getServiceDI(ctx)
	if di == nil {
		return nil, status.Error(codes.Internal, "di was not found")
	}

	dbclt := db.GetMgoDBClientByCtx(ctx)
	l := log.GetLogByCtx(ctx)

	result, err := diutil.RedisCtxHandler(ctx, di.GetServiceName(), func(redis db.RedisClient) (any, error) {
		otpModel := model.NewOtpModel(ctx, dbclt.GetCoreDB(), l, redis)
		resp, err := otpModel.ForgetPwd(&dao.ForgetPwd{Host: req.Host, Email: req.Email})
		if err != nil {
			return nil, err
		}
		return resp, nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, "redis handler fail: "+err.Error())
	}

	forgetPwdResp := result.(*model.ForgetPwdResp)
	return &pb.ForgetPasswordResponse{
		Email: forgetPwdResp.User.Email,
		Name:  forgetPwdResp.User.Name,
		Code:  forgetPwdResp.Code,
	}, nil
}
