package api

import (
	"net/http"

	"auth/model"

	api "github.com/94peter/sterna/api"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/gin-gonic/gin"
)

func NewInternalAPI(service string) api.GinAPI {
	return &internalAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type internalAPI struct {
	api.ErrorOutputAPI
}

func (a *internalAPI) GetName() string {
	return "internal"
}

func (a *internalAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		{Method: "POST", Path: "/internal/user/channel", Handler: a.getUserChannelHandler, Auth: true},
	}
}

func (a *internalAPI) getUserChannelHandler(c *gin.Context) {
	account := c.Param("acc")
	log := log.GetLogByGin(c)
	dbclt := db.GetMgoDBClientByGin(c)
	crud := model.NewCRUD(c, dbclt.GetCoreDB(), log)
	user, err := crud.FindUser(account)
	result := make(map[string]interface{})
	if err != nil || user == nil {
		result["channel"] = "default"
	} else {
		result["channel"] = user.Channels[0]
	}
	c.JSON(http.StatusOK, result)
}
