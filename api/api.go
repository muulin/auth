package api

import (
	"auth/dao"

	"github.com/94peter/sterna"
	"github.com/94peter/sterna/auth"
	"github.com/gin-gonic/gin"
)

type apiDI interface {
	GetPlatforms() dao.Platforms
	GetServiceName() string
	auth.JwtDI
}

func getApiDI(c *gin.Context) apiDI {
	servDi, ok := c.Get(string(sterna.CtxServDiKey))
	if !ok {
		return nil
	}
	return servDi.(apiDI)
}
