package api

import (
	"fmt"
	"net/http"

	"auth/dao"
	"auth/input"
	"auth/model"

	"bitbucket.org/muulin/interlib/diutil"
	api "github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"github.com/gin-gonic/gin"
)

// func getRedis(c *gin.Context) (db.RedisClient, error) {
// 	redisDI := util.GetCtxVal(c.Request, sterna.CtxServDiKey).(db.RedisDI)
// 	if redisDI == nil {
// 		return nil, errors.New("redis no set")
// 	}
// 	return redisDI.NewRedisClientDB(c.Request.Context(), redisDI.GetDB(interTypes.Redis_DB_Auth))
// }

func NewAuthAPI(service string) api.GinAPI {
	return &authAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type authAPI struct {
	api.ErrorOutputAPI
}

func (a *authAPI) GetName() string {
	return "auth"
}

func (a *authAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		// 新增系統管理使用者
		{Method: "POST", Path: "/v5/admin/user", Handler: a.createAdminUserEndpoint, Auth: false},
		{Method: "POST", Path: "/v5/admin/user/platform", Handler: a.addUserPlatform, Auth: true},
		// 註冊
		// {Method: "POST", Path: "/v5/register", Handler: a.registerEndpoint, Auth: false},
		// 登入
		{Method: "POST", Path: "/v5/login", Handler: a.loginEndpoint, Auth: false},
		// otp登入
		{Method: "POST", Path: "/v5/otp/login", Handler: a.otpLoginEndpoint, Auth: false},
		// 取得使用者資料
		{Method: "GET", Path: "/v5/info", Handler: a.getUserInfoEndpoint, Auth: true},
		{Method: "PUT", Path: "/v5/pwd/otp", Handler: a.otpChangePwdEndpoint, Auth: true},

		// token 驗證
		// {Method: "GET", Path: "/v5/token/:CID", Handler: a.tokenEndpoint, Auth: true},
	}
}

func (a *authAPI) createAdminUserEndpoint(c *gin.Context) {
	di := getApiDI(c)
	in := &input.AdminRegister{}
	err := util.ParserDataRequest(c.Request, in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	if err = in.Validate(); err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	found := false
	for _, p := range di.GetPlatforms() {
		if p.Key == in.Platform {
			found = true
			break
		}
	}
	if !found {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, "invlid platform"))
		return
	}

	mgoClient := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)
	rm := model.NewRegisterAccount(c, mgoClient.GetCoreDB(), l)

	err = rm.AdminRegister(in)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}

	c.String(http.StatusOK, "OK")
}

func (a *authAPI) addUserPlatform(c *gin.Context) {
	in := &input.AddUserPlatform{}
	err := util.ParserDataRequest(c.Request, in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	if err = in.Validate(); err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	platformList := getApiDI(c).GetPlatforms()
	found := false
	for _, p := range platformList {
		if p.Key == in.Platform {
			found = true
			break
		}
	}
	if !found {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, "invlid platform"))
		return
	}

	err = model.NewCRUD(c, db.GetMgoDBClientByGin(c).GetCoreDB(), log.GetLogByGin(c)).AddUserPerm(in)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}
	c.Writer.WriteString("ok")
}
func (a *authAPI) loginEndpoint(c *gin.Context) {
	platform := c.GetHeader("X-PLATFORM")
	in := &input.Login{}
	err := util.ParserDataRequest(c.Request, in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	if err = in.Validate(); err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	log := log.GetLogByReq(c.Request)
	mgom := mgom.NewMgoModelByReq(c.Request, db.CoreDB)
	userPwd := model.NewUserPwdLogin(mgom, log)
	user, err := userPwd.Login(util.GetHost(c.Request), in.Account, in.Pwd, platform)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable, err.Error()+" (invalid username / password)"))
		return
	}

	// if in.Push != "" {
	// 	notify := model.NewUserNotify(mgom)
	// 	err = notify.AddNotify(user.ID, in.Platform, in.Push)
	// 	if err != nil {
	// 		a.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable, err.Error()))
	// 		return
	// 	}
	// }

	userToken := model.NewJwtUserToken(diutil.GetJwtDIByRequest(c.Request).NewJwt())
	token, err := userToken.Generate(user)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable, "Generate toke failed: "+err.Error()))
		return
	}
	log.Debug(fmt.Sprintf("Login token: %s", *token))
	userMeta := model.NewMongoUserMeta(mgom)
	meta, err := userMeta.FindByID(user.ID)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable, err.Error()))
		return
	}

	meta["utoken"] = *token
	meta["platform"] = map[string]any{
		"name":  user.Platform.Platform,
		"roles": user.Platform.Roles,
	}
	meta["state"] = user.State
	c.JSON(http.StatusOK, meta)
}

func (app *authAPI) otpLoginEndpoint(c *gin.Context) {
	platform := c.GetHeader("X-Platform")
	in := &dao.OtpLogin{
		Host: util.GetHost(c.Request),
	}

	err := c.BindJSON(in)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	if err = in.Validate(); err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	log := log.GetLogByGin(c)
	mgoClient := db.GetMgoDBClientByGin(c)
	di := getApiDI(c)
	result, err := diutil.RedisGinHandler(c, di.GetServiceName(), func(redis db.RedisClient) (any, error) {
		userPwd := model.NewOtpModel(c.Request.Context(), mgoClient.GetCoreDB(), log, redis)
		user, err := userPwd.OtpLogin(in.Host, in.Email, in.Code, platform)
		if err != nil {
			return nil, err
		}
		return user, nil
	})

	if err != nil {
		app.GinOutputErr(c, err)
		return
	}
	user := result.(*dao.LoginUser)
	userToken := model.NewJwtUserToken(diutil.GetJwtDIByRequest(c.Request).NewJwt())
	token, err := userToken.Generate(user)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusInternalServerError, "Token generate fail"))
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"utoken": *token,
		"name":   user.Name,
		"state":  user.State,
		"platform": map[string]any{
			"name":  user.Platform.Platform,
			"roles": user.Platform.Roles,
		},
	})
}

func (app *authAPI) registerEndpoint(c *gin.Context) {
	// cb := &input.Register{
	// 	Lang: "zh-TW",
	// }

	// err := util.ParserDataRequest(c.Request, cb)
	// if err != nil {
	// 	app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
	// 	return
	// }
	// if err = cb.Validate(); err != nil {
	// 	app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
	// 	return
	// }

	// mgoClient := db.GetMgoDBClientByGin(c)
	// l := log.GetLogByGin(c)

	// rm := model.NewRegisterAccount(c, mgoClient.GetCoreDB(), l)
	// im := model.NewIntivation(c.Request.Context(), mgoClient.GetCoreDB(), l)
	// err = rm.Regist(im, cb.ToDaoRegister())
	// if err != nil {
	// 	app.GinOutputErr(c, err)
	// 	return
	// }

	// c.String(http.StatusOK, "OK")
}

func (a *authAPI) getUserInfoEndpoint(c *gin.Context) {
	info := auth.GetTargetUserInfo(c.Request)
	logger := log.GetLogByReq(c.Request)
	mgoClient := db.GetMgoDBClientByReq(c.Request)

	mgoModel := mgom.NewMgoModel(c.Request.Context(), mgoClient.GetCoreDB(), logger)
	uid, _ := sternaDao.GetObjectID(info.GetId())

	metaCrud := model.NewMongoUserMeta(mgoModel)
	userMeta, err := metaCrud.FindByID(uid)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}

	userCrud := model.NewCRUD(c.Request.Context(), mgoClient.GetCoreDB(), logger)
	user, err := userCrud.FindByID(uid)
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}

	userMeta["channel"] = info.Target()
	userMeta["id"] = user.ID.Hex()
	userMeta["name"] = user.Name
	userMeta["account"] = user.Email
	userMeta["roles"] = info.GetPerm()

	c.JSON(http.StatusOK, userMeta)
}

func (a *authAPI) otpChangePwdEndpoint(c *gin.Context) {
	in := &dao.OtpChangePwd{}
	err := c.BindJSON(in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	err = in.Validate()
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	mgoClient := db.GetMgoDBClientByReq(c.Request)
	l := log.GetLogByReq(c.Request)
	u := auth.GetUserInfo(c.Request)
	di := getApiDI(c)

	_, err = diutil.RedisGinHandler(c, di.GetServiceName(), func(redis db.RedisClient) (any, error) {
		otpModel := model.NewOtpModel(c.Request.Context(), mgoClient.GetCoreDB(), l, redis)
		err = otpModel.OtpChangePwd(u, in)
		if err != nil {
			return nil, err
		}
		return nil, nil
	})
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}

	c.String(http.StatusOK, "OK")
}
