package api

import (
	"net/http"

	"auth/dao"
	"auth/input"
	"auth/model"

	api "github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"github.com/gin-gonic/gin"
)

func NewV1AuthAPI(service string) api.GinAPI {
	return &v1AuthAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type v1AuthAPI struct {
	api.ErrorOutputAPI
}

func (a *v1AuthAPI) GetName() string {
	return "V1auth"
}

func (a *v1AuthAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		// admin登入
		{Method: "POST", Path: "/v1/:platform/login", Handler: a.loginEndpoint, Auth: false},
		// 修改密碼
		{Method: "POST", Path: "/v1/pwd", Handler: a.changePwdEndpoint, Auth: true},
	}
}

func (a *v1AuthAPI) loginEndpoint(c *gin.Context) {
	platform := c.Param("platform")

	in := &input.Login{}
	err := util.ParserDataRequest(c.Request, in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	if err = in.Validate(); err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	log := log.GetLogByGin(c)
	dbclt := db.GetMgoDBClientByGin(c)
	mgom := mgom.NewMgoModel(c, dbclt.GetCoreDB(), log)
	userPwd := model.NewUserPwdLogin(mgom, log)
	user, err := userPwd.Login(util.GetHost(c.Request), in.Account, in.Pwd, platform)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusNotFound, "login fail: "+err.Error()))
		return
	}

	userToken := model.NewJwtUserToken(getApiDI(c).NewJwt())
	token, err := userToken.Generate(user)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable, "Generate toke failed: "+err.Error()))
		return
	}

	c.JSON(http.StatusOK, map[string]any{
		"token": token,
		"name":  user.Name,
		"roles": user.Platform.Roles,
	})
}

func (a *v1AuthAPI) changePwdEndpoint(c *gin.Context) {
	in := &input.ChangePwd{}

	err := util.ParserDataRequest(c.Request, in)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	if err = in.Validate(); err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	user := auth.GetUserInfo(c.Request)
	mgom := mgom.NewMgoModelByReq(c.Request, db.CoreDB)
	userPwd := model.NewUserPwdLogin(mgom, log.GetLogByReq(c.Request))
	err = userPwd.ChangePwd(user, &dao.ChangePwd{
		OldPwd: in.OldPwd,
		NewPwd: in.NewPwd,
	})
	if err != nil {
		a.GinOutputErr(c, err)
		return
	}

	c.String(http.StatusOK, "OK")
}
