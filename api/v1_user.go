package api

import (
	"net/http"

	"auth/dao/mongo"
	"auth/input"
	"auth/model"

	"go.mongodb.org/mongo-driver/bson/primitive"

	api "github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/gin-gonic/gin"
)

func NewV1UserAPI(service string) api.GinAPI {
	return &v1UserAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type v1UserAPI struct {
	api.ErrorOutputAPI
}

func (a *v1UserAPI) GetName() string {
	return "V1_User"
}

func (a *v1UserAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		// 使用者清單
		{Method: "GET", Path: "/v1/user", Handler: a.getHandler, Auth: true},
		// 使用者明細
		{Method: "GET", Path: "/v1/user/:id", Handler: a.getDetailHandler, Auth: true},
		// 更新使用者
		{Method: "PUT", Path: "/v1/user/:id", Handler: a.putHandler, Auth: true},
		// 刪除使用者
		{Method: "DELETE", Path: "/v1/user/:id", Handler: a.deleteHandler, Auth: true},
	}
}

func (a *v1UserAPI) getHandler(c *gin.Context) {
	log := log.GetLogByGin(c)
	dbclt := db.GetMgoDBClientByGin(c)
	crud := model.NewCRUD(c, dbclt.GetCoreDB(), log)
	users, err := crud.All()
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusNotFound, err.Error()))
		return
	}

	result, count := sternaDao.Format(users, func(i interface{}) map[string]interface{} {
		if u, ok := i.(*mongo.User); ok {
			return map[string]interface{}{
				"id":          u.ID.Hex(),
				"account":     u.Email,
				"description": u.Name,
			}
		}
		return nil
	})

	if count == 0 {
		c.Writer.WriteHeader(http.StatusNoContent)
		return
	}

	c.JSON(http.StatusOK, map[string]any{
		"raws":     result,
		"allPages": 1,
		"page":     1,
		"limit":    count,
	})
}

func (a *v1UserAPI) getDetailHandler(c *gin.Context) {
	id := c.Param("id")
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	log := log.GetLogByGin(c)
	dbclt := db.GetMgoDBClientByGin(c)
	crud := model.NewCRUD(c, dbclt.GetCoreDB(), log)
	user, err := crud.FindByID(oid)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusNotFound, err.Error()))
		return
	}
	c.JSON(http.StatusOK, map[string]any{
		"id":          user.ID.Hex(),
		"account":     user.Email,
		"name":        user.Name,
		"description": user.Name,
	})
}

func (a *v1UserAPI) putHandler(c *gin.Context) {
	id := c.Param("id")
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	var updateUser input.UpdateUser
	err = c.Bind(&updateUser)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	log := log.GetLogByGin(c)
	dbclt := db.GetMgoDBClientByGin(c)
	crud := model.NewCRUD(c, dbclt.GetCoreDB(), log)
	err = crud.UpdateUser(oid, &updateUser)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusNotFound, "update user fail: "+err.Error()))
		return
	}
	c.Writer.WriteString("ok")
}

func (a *v1UserAPI) deleteHandler(c *gin.Context) {
	id := c.Param("id")
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	log := log.GetLogByGin(c)
	dbclt := db.GetMgoDBClientByGin(c)
	crud := model.NewCRUD(c, dbclt.GetCoreDB(), log)
	err = crud.Delete(oid)
	if err != nil {
		a.GinOutputErr(c, apiErr.New(http.StatusNotFound, "delete user fail: "+err.Error()))
		return
	}
	c.Writer.WriteString("ok")
}
