package api

import (
	"net/http"
	"net/url"

	"auth/dao"
	"auth/input"
	"auth/model"

	api "github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"github.com/gin-gonic/gin"
)

// func getRedis(c *gin.Context) (db.RedisClient, error) {
// 	redisDI := util.GetCtxVal(c.Request, sterna.CtxServDiKey).(db.RedisDI)
// 	if redisDI == nil {
// 		return nil, errors.New("redis no set")
// 	}
// 	return redisDI.NewRedisClientDB(c.Request.Context(), redisDI.GetDB(interTypes.Redis_DB_Auth))
// }

func NewV3AuthAPI(service string) api.GinAPI {
	return &v3AuthAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type v3AuthAPI struct {
	api.ErrorOutputAPI
}

func (a *v3AuthAPI) GetName() string {
	return "V3auth"
}

func (a *v3AuthAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		// 登入
		{Method: "POST", Path: "/v3/login", Handler: a.loginHandler, Auth: false},
	}
}

func (app *v3AuthAPI) registerHandler(c *gin.Context) {
	in := &input.Register{
		InvitationID: c.PostForm("invitationId"),
		Name:         c.PostForm("name"),
		Pwd:          c.PostForm("pwd"),
		Lang:         "zh-tw",
	}

	if err := in.Validate(); err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}

	mgoClient := db.GetMgoDBClientByReq(c.Request)
	l := log.GetLogByReq(c.Request)

	rm := model.NewRegisterAccount(c.Request.Context(), mgoClient.GetCoreDB(), l)
	im := model.NewIntivation(c.Request.Context(), mgoClient.GetCoreDB(), l)
	err := rm.Regist(im, in.ToDaoRegister())
	myurl, err := url.Parse(c.PostForm("redirectUrl"))
	if err != nil {
		query := myurl.Query()
		query.Add("errMsg", err.Error())
		myurl.RawQuery = query.Encode()
		c.Redirect(http.StatusTemporaryRedirect, myurl.String())
		return
	}
	c.Redirect(http.StatusTemporaryRedirect, myurl.String())
}

func (app *v3AuthAPI) loginHandler(c *gin.Context) {
	platForm := c.GetHeader("X-Platform")
	in := &input.V3_Login{}
	err := util.ParserDataRequest(c.Request, in)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	if err = in.Validate(); err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	mgoClient := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)

	mgomModel := mgom.NewMgoModel(c, mgoClient.GetCoreDB(), l)
	loginModel := model.NewUserPwdLogin(mgomModel, l)
	loginUser, err := loginModel.Login(util.GetHost(c.Request), in.Account, in.Pwd, platForm)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusNotFound, err.Error()))
		return
	}

	userToken := model.NewJwtUserToken(getApiDI(c).NewJwt())
	token, err := userToken.Generate(loginUser)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable, "Generate toke failed: "+err.Error()))
		return
	}
	// update notify
	notifyModel := model.NewUserNotify(mgomModel)
	err = notifyModel.AddNotify(
		loginUser.ID, in.PhoneId,
		dao.UserNotifyPlatform(in.Platform),
		in.Push,
	)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable,
			"add notify fail: "+err.Error()))
		return
	}
	// get user folder
	folderModel := model.NewUserFolder(mgomModel)
	folders, err := folderModel.Get(loginUser.ID)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusServiceUnavailable,
			"get folder fail: "+err.Error()))
		return
	}
	folderOutput := make([]map[string]any, len(folders))
	for i, f := range folders {
		folderOutput[i] = map[string]any{
			"name":        f.GetName(),
			"projectList": f.GetContent(),
		}
	}

	c.JSON(http.StatusOK, map[string]any{
		"token":         token,
		"name":          loginUser.Name,
		"state":         loginUser.State,
		"roles":         loginUser.Platform.Roles,
		"projectFolder": folderOutput,
	})
}
