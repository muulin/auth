package api

import (
	"net/http"

	"auth/input"
	"auth/model"

	api "github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/gin-gonic/gin"
)

// func getRedis(c *gin.Context) (db.RedisClient, error) {
// 	redisDI := util.GetCtxVal(c.Request, sterna.CtxServDiKey).(db.RedisDI)
// 	if redisDI == nil {
// 		return nil, errors.New("redis no set")
// 	}
// 	return redisDI.NewRedisClientDB(c.Request.Context(), redisDI.GetDB(interTypes.Redis_DB_Auth))
// }

func NewV3UserAPI(service string) api.GinAPI {
	return &v3UserAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type v3UserAPI struct {
	api.ErrorOutputAPI
}

func (a *v3UserAPI) GetName() string {
	return "V3user"
}

func (a *v3UserAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		// 登入畫面
		{Method: "POST", Path: "/v3/user/project-folder", Handler: a.userProjectFolderEndpoint, Auth: true},
	}
}

func (app *v3UserAPI) userProjectFolderEndpoint(c *gin.Context) {
	var in input.CreateUserProjectFolders

	err := c.Bind(&in)
	if err != nil {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, err.Error()))
		return
	}
	mgoClient := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)
	user := auth.GetUserByGin(c)

	mgomModel := mgom.NewMgoModel(c, mgoClient.GetCoreDB(), l)
	rm := model.NewUserFolder(mgomModel)

	err = rm.Upser(in, user)
	if err != nil {
		app.GinOutputErr(c, err)
		return
	}

	c.Writer.WriteString("ok")
}
