package api

import (
	"net/http"

	"auth/dao/mongo"
	"auth/model"

	api "github.com/94peter/sterna/api"
	apiErr "github.com/94peter/sterna/api/err"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/gin-gonic/gin"
)

// func getRedis(c *gin.Context) (db.RedisClient, error) {
// 	redisDI := util.GetCtxVal(c.Request, sterna.CtxServDiKey).(db.RedisDI)
// 	if redisDI == nil {
// 		return nil, errors.New("redis no set")
// 	}
// 	return redisDI.NewRedisClientDB(c.Request.Context(), redisDI.GetDB(interTypes.Redis_DB_Auth))
// }

func NewV2UserAPI(service string) api.GinAPI {
	return &v2UserAPI{
		ErrorOutputAPI: api.NewErrorOutputAPI(service),
	}
}

type v2UserAPI struct {
	api.ErrorOutputAPI
}

func (a *v2UserAPI) GetName() string {
	return "V2user"
}

func (a *v2UserAPI) GetAPIs() []*api.GinApiHandler {
	return []*api.GinApiHandler{
		// 登入畫面
		{Method: "POST", Path: "/v2/user/search", Handler: a.userSearchEndpoint, Auth: false},
	}
}

func (app *v2UserAPI) userSearchEndpoint(c *gin.Context) {
	key, ok := c.GetQuery("key")
	if !ok || key == "" {
		app.GinOutputErr(c, apiErr.New(http.StatusBadRequest, "key is empty"))
		return
	}

	mgoClient := db.GetMgoDBClientByGin(c)
	l := log.GetLogByGin(c)

	rm := model.NewCRUD(c, mgoClient.GetCoreDB(), l)

	result, err := rm.Search(key)
	if err != nil {
		app.GinOutputErr(c, err)
		return
	}
	format, count := sternaDao.Format(result, func(i interface{}) map[string]interface{} {
		if user, ok := i.(*mongo.User); ok {
			return map[string]interface{}{
				"id":      user.ID.Hex(),
				"account": user.Email,
				"name":    user.Name,
			}
		}
		return nil
	})
	if count == 0 {
		c.Writer.WriteHeader(http.StatusNoContent)
		return
	}
	c.JSON(http.StatusOK, format)
}
