package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"

	myapi "auth/api"
	dao "auth/dao"
	"auth/model"
	"auth/mygrpc"

	"bitbucket.org/muulin/interlib/auth/pb"
	coreInterceptor "bitbucket.org/muulin/interlib/core/interceptor"
	"github.com/94peter/sterna"
	"github.com/94peter/sterna/api"
	sternaMid "github.com/94peter/sterna/api/mid"
	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/mystorage"
	"github.com/94peter/sterna/util"

	sternaLog "github.com/94peter/sterna/log"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	envMode = flag.String("em", "local", "local, container")

	v         = flag.Bool("v", false, "version")
	Version   = "1.0.0"
	BuildTime = time.Now().Local().GoString()
)

func main() {
	flag.Parse()

	if *v {
		fmt.Println("Version: " + Version)
		fmt.Println("Build Time: " + BuildTime)
		return
	}

	if *envMode == "local" {
		err := godotenv.Load(".env")
		if err != nil {
			fmt.Println("No .env file")
		}
	}
	env := os.Getenv("environment")
	confPath := os.Getenv("CONF_PATH")
	serviceName := os.Getenv(("SERVICE"))
	if *envMode == "local" {
		confPath = util.StrAppend(confPath, env, "/")
	}

	hdstore := mystorage.NewHdStorage(confPath)
	di := &di{
		serviceName: serviceName,
	}
	confByte, err := hdstore.Get("config.yml")
	if err != nil {
		panic(err)
	}
	sterna.InitConfByByte(confByte, di)
	if err = di.IsConfEmpty(); err != nil {
		panic("config setting error: " + err.Error())
	}
	go func() {
		for {
			runGrpc(di)
			time.Sleep(time.Second)
		}
	}()
	runApi(di)
}

func runGrpc(mydi *di) {

	port := ":" + os.Getenv("GRPC_PORT")

	dbCoreInter := coreInterceptor.NewLocalDBInterceptor(mydi)
	log.Println("run api port: ", port)
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				dbCoreInter.StreamServerInterceptor(),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				dbCoreInter.UnaryServerInterceptor(),
			)),
	)
	reflection.Register(s)
	pb.RegisterAuthServiceServer(s, mygrpc.NewService())
	log.Printf("app gRPC server is running [%s].", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func runApi(mydi *di) {
	port := os.Getenv("API_PORT")
	ginMode := os.Getenv("GIN_MODE")
	authMode := os.Getenv("API_AUTH_MODE")
	logLevel := os.Getenv("LOG_LEVEL")
	serviceName := mydi.GetServiceName()
	trustedProxiesEnv := os.Getenv("TRUSTED_PROXIES")
	if trustedProxiesEnv == "" {
		trustedProxiesEnv = "127.0.0.1"
	}

	prxies := strings.Split(trustedProxiesEnv, ",")

	var authMiddle sternaMid.AuthGinMidInter
	if authMode == "mock" {
		authMiddle = sternaMid.NewMockAuthMid()
	} else {
		authMiddle = sternaMid.NewGinBearAuthMid(serviceName, ginMode == "release")
	}

	mids := []sternaMid.GinMiddle{
		sternaMid.NewGinFixDiMid(mydi, serviceName),
		sternaMid.NewGinTokenParserMid(serviceName, model.NewJwtUserToken(mydi).GetAuthTokenParser()),
		authMiddle,
		sternaMid.NewGinDBMid(serviceName),
	}
	if logLevel == "debug" {
		mids = append([]sternaMid.GinMiddle{sternaMid.NewGinDebugMid(serviceName)}, mids...)
	}

	log.Printf("run api port [%s], auth mode is [%s]", port, authMode)
	log.Fatal(api.NewGinApiServer(ginMode).
		SetAuth(authMiddle).
		Middles(
			mids...,
		).AddAPIs(
		api.NewHealthAPI(serviceName),
		myapi.NewAuthAPI(serviceName),
		myapi.NewV1UserAPI(serviceName),
		myapi.NewV1AuthAPI(serviceName),
		myapi.NewV2UserAPI(serviceName),
		myapi.NewV3AuthAPI(serviceName),
		myapi.NewV3UserAPI(serviceName),
		myapi.NewInternalAPI(serviceName),
	).SetTrustedProxies(prxies).Run(port).Error())
}

type di struct {
	serviceName          string
	*db.MongoConf        `yaml:"mongo,omitempty"`
	sternaLog.LoggerConf `yaml:"log"`
	*auth.JwtConf        `yaml:"jwtConf"`
	*db.RedisConf        `yaml:"redis,omitempty"`
	dao.Platforms        `yaml:"platforms"`
}

func (d *di) IsConfEmpty() error {
	if d.MongoConf == nil {
		return errors.New("mongo no set")
	}
	if os.Getenv("LOG_TARGET") == "fluent" &&
		d.LoggerConf.FluentLog == nil {
		return errors.New("log.FluentLog no set")
	}

	if d.JwtConf == nil {
		return errors.New("jwtConf no set")
	}
	if d.RedisConf == nil {
		return errors.New("redis no set")
	}
	return nil
}

func (d *di) GetServiceName() string {
	return d.serviceName
}

func (d *di) GetPlatforms() dao.Platforms {
	return d.Platforms
}
