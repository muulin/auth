package model

import (
	"context"
	"net/http"

	mon "auth/dao/mongo"
	"auth/input"

	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type crud struct {
	dbmodel mgom.MgoDBModel
	log     log.Logger
}

func NewCRUD(ctx context.Context, db *mongo.Database, l log.Logger) Crud {
	return &crud{
		dbmodel: mgom.NewMgoModel(ctx, db, l),
		log:     l,
	}
}

func NewCrudByModel(dbModel mgom.MgoDBModel, l log.Logger) Crud {
	return &crud{
		dbmodel: dbModel,
		log:     l,
	}
}

type Crud interface {
	FindByID(id primitive.ObjectID) (*mon.User, error)
	FindUser(email string) (*mon.User, error)
	All() ([]*mon.User, error)
	// UpdatePerm(auth.ReqUser, *dao.Invitation) error
	UpdateUser(id primitive.ObjectID, in *input.UpdateUser) error
	Delete(id primitive.ObjectID) error
	AccountExists(accounts []string) (map[string]bool, error)
	Search(key string) ([]*mon.User, error)
	AddUserPerm(in *input.AddUserPlatform) error
}

func (impl *crud) AccountExists(accounts []string) (map[string]bool, error) {
	d := &mon.User{}

	results, err := impl.dbmodel.Find(d, bson.M{
		"email": bson.M{"$in": accounts},
	})
	if err != nil {
		return nil, err
	}
	m := map[string]bool{}
	for _, u := range results.([]*mon.User) {
		m[u.Email] = true
	}
	for _, acc := range accounts {
		if _, ok := m[acc]; !ok {
			m[acc] = false
		}
	}
	return m, nil
}

func (impl *crud) FindByID(id primitive.ObjectID) (*mon.User, error) {
	d := &mon.User{ID: id}
	err := impl.dbmodel.FindByID(d)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, apiErr.New(http.StatusNotFound, "user not found")
		}
		return nil, err
	}

	return d, nil
}

func (impl *crud) FindUser(email string) (*mon.User, error) {
	d := &mon.User{}

	err := impl.dbmodel.FindOne(d, bson.M{
		"email": email,
	})
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, apiErr.New(http.StatusNotFound, "user not found")
		}
		return nil, err
	}

	return d, nil
}

func (impl *crud) All() ([]*mon.User, error) {
	var user *mon.User
	users, err := impl.dbmodel.Find(user, bson.M{})
	if err != nil {
		return nil, err
	}
	return users.([]*mon.User), nil
}

func (impl *crud) UpdateUser(id primitive.ObjectID, in *input.UpdateUser) error {
	user := &mon.User{
		ID: id,
	}
	_, err := impl.dbmodel.UpdateOne(user, bson.D{
		{Key: "name", Value: in.Name},
	}, nil)
	return err
}

func (impl *crud) Delete(id primitive.ObjectID) error {
	_, err := impl.dbmodel.RemoveByID(&mon.User{ID: id}, nil)
	return err
}

func (impl *crud) Search(key string) ([]*mon.User, error) {
	d := &mon.User{}

	result, err := impl.dbmodel.Find(d, bson.M{
		"$text": bson.M{"$search": key},
	})
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, apiErr.New(http.StatusNotFound, "user not found")
		}
		return nil, err
	}

	return result.([]*mon.User), nil
}

func (impl *crud) AddUserPerm(in *input.AddUserPlatform) error {
	u := &mon.User{}
	err := impl.dbmodel.FindOne(u, bson.M{
		"email": in.Account,
	})
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return apiErr.New(http.StatusNotFound, "user not found")
		}
		return err
	}
	u.AddPlatform(in.Platform, in.Roles)
	_, err = impl.dbmodel.UpdateOne(u, bson.D{
		{Key: "userservperm", Value: u.UserServPerm},
	}, nil)
	return err
}

// func (impl *crud) UpdatePerm(u auth.ReqUser, invitaion *dao.Invitation) error {
// 	uID, err := sternaDao.GetObjectID(u.GetId())
// 	if err != nil {
// 		return err
// 	}

// 	user, err := impl.FindByID(uID)
// 	if err != nil {
// 		return err
// 	}

// 	if user.Email != invitaion.Email {
// 		return apiErr.New(http.StatusForbidden, "invitation email and token not match")
// 	}

// 	user.UpdateServPerm(invitaion)

// 	_, err = impl.dbmodel.UpdateOne(user, bson.D{
// 		{Key: "userservperm", Value: user.UserServPerm},
// 	}, u)
// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }
