package model

import (
	"context"
	"errors"
	"net/http"
	"os"

	dao "auth/dao"
	input "auth/input"
	types "auth/types"

	mongoDao "auth/dao/mongo"

	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	OtpPeriodSecs = 600
)

func NewRegisterAccount(ctx context.Context, db *mongo.Database, l log.Logger) RegisterModel {
	return &registerAccount{
		dbmodel: mgom.NewMgoModel(ctx, db, l),
		log:     l,
	}
}

type RegisterModel interface {
	AdminRegister(in *input.AdminRegister) error
	Regist(intivation IntivationModel, user *dao.Register) error
	// IsExist(email string) bool
	// VerifyEmail(l *dao.VerifyEmail) error
	// ResendVerify(grpClt message.MessageClient, tpl MailTpl, host, email string) error
}

type registerAccount struct {
	dbmodel mgom.MgoDBModel
	log     log.Logger
}

func (r *registerAccount) AdminRegister(in *input.AdminRegister) error {
	if in.Code != os.Getenv("Auth_Code") {
		return apiErr.New(http.StatusForbidden, "invalid code")
	}
	userModel := NewCrudByModel(r.dbmodel, r.log)
	user, _ := userModel.FindUser(in.Email)
	if user != nil {
		return apiErr.New(http.StatusConflict, "email duplicated")
	}
	daoUser := &dao.User{
		Name:  in.Name,
		Type:  dao.MainUser,
		Email: in.Email,
		Pwd:   util.MD5(in.Pwd),
		Lang:  in.Lang,
		UserServPerm: []*dao.ServicePerm{
			{
				Platform: in.Platform,
				Roles:    in.Roles,
			},
		},
		Enable:          true,
		IsEmailVerified: true,
		Channels:        in.Channels,
	}
	u := &mongoDao.User{
		User: daoUser,
	}
	_, err := r.dbmodel.Save(u, daoUser)
	return err
}

// func (r *otpRegisterAccount) IsExist(email string) bool {
// 	acc := &mongoDao.User{}
// 	err := r.dbmodel.FindOne(acc, bson.M{"email": email})
// 	if err != nil {
// 		return false
// 	}
// 	return !acc.ID.IsZero()
// }

func (r *registerAccount) Regist(intivation IntivationModel, l *dao.Register) error {
	in, err := intivation.FindByID(l.InvitationID)
	if err != nil {
		return apiErr.NewWithKey(http.StatusNotFound, "intivation not exist:"+err.Error(), "intivationNotExist")
	}

	if in.State != types.InvitationStateNew {
		return apiErr.New(http.StatusForbidden, "invitation cannot be access")
	}

	userModel := NewCrudByModel(r.dbmodel, r.log)
	user, _ := userModel.FindUser(in.Email)
	if user != nil {
		return apiErr.New(http.StatusConflict, "email duplicated")
	}

	daoUser := &dao.User{
		Name:            l.Name,
		Email:           in.Email,
		Pwd:             util.MD5(l.Pwd),
		Lang:            l.Lang,
		UserServPerm:    []*dao.ServicePerm{},
		Enable:          true,
		IsEmailVerified: true,
	}
	u := &mongoDao.User{
		User: daoUser,
	}
	_, err = r.dbmodel.Save(u, daoUser)
	userMeta := &mongoDao.UserMeta{
		ID:       u.ID,
		UserMeta: l.Meta,
	}
	if err != nil {
		return err
	}

	_, err = r.dbmodel.Save(userMeta, daoUser)
	if err != nil {
		return errors.New("userMeta save error: " + err.Error())
	}
	return nil
}

// func (r *otpRegisterAccount) VerifyEmail(l *dao.VerifyEmail) error {
// 	docUser := &mongoDao.User{}
// 	err := r.dbmodel.FindOne(docUser, bson.M{"email": l.Email})
// 	if err != nil {
// 		return api.NewApiError(http.StatusNotFound, err.Error())
// 	}
// 	totp := auth.NewTotp(l.Host, l.Email, docUser.Pwd, otpPeriodSecs)
// 	valid, err := totp.ValidateCode(l.Code)
// 	if err != nil {
// 		return err
// 	}
// 	if !valid {
// 		return api.NewApiError(http.StatusBadRequest, "invalid code")
// 	}
// 	docUser.IsEmailVerified = true
// 	docUser.Enable = true
// 	_, err = r.dbmodel.UpdateOne(docUser, bson.D{
// 		{Key: "isemailverified", Value: true},
// 		{Key: "enable", Value: true},
// 	}, docUser)
// 	return err
// }

// func (r *otpRegisterAccount) ResendVerify(grpClt message.MessageClient, tpl MailTpl, host, email string) error {
// 	docUser := &mongoDao.User{}
// 	err := r.dbmodel.FindOne(docUser, bson.M{"email": email})
// 	if err != nil {
// 		r.log.Warn("email not found: " + email)
// 		return nil
// 	}
// 	if docUser.IsEmailVerified {
// 		r.log.Info("email has verified: " + email)
// 		return nil
// 	}
// 	totp := auth.NewTotp(host, email, docUser.Pwd, otpPeriodSecs)
// 	code, err := totp.GenerateCode()
// 	if err != nil {
// 		return err
// 	}
// 	result, err := tpl.GetContent(dao.MailTplRegister, struct {
// 		Name string
// 		Code string
// 	}{
// 		Name: email,
// 		Code: code,
// 	})
// 	if err != nil {
// 		return errors.New("mail tpl not set")
// 	}
// 	r.log.Debug(result.Html)
// 	return mailserv.Subject(result.Title).Html(result.Html).
// 		PlaintText(result.Txt).SendSingle(email, email)
// }
