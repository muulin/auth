package model

import (
	"bytes"
	"context"
	"errors"
	"text/template"

	dao "auth/dao"
	mongoDao "auth/dao/mongo"

	coreDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/model/mgom"

	"github.com/94peter/sterna/log"

	"github.com/94peter/sterna/util"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/html"
)

type MailTplResult struct {
	Title string
	Html  string
	Txt   string
}

func (result *MailTplResult) render(data interface{}) error {
	t, err := template.New("mailTplResult").Parse(result.Html)
	if err != nil {
		return err
	}
	var tpl bytes.Buffer
	if err = t.Execute(&tpl, data); err != nil {
		return err
	}
	result.Html = tpl.String()
	tpl.Reset()

	t, err = t.Parse(result.Title)
	if err = t.Execute(&tpl, data); err != nil {
		return err
	}
	result.Title = tpl.String()
	tpl.Reset()

	t, err = t.Parse(result.Txt)
	if err = t.Execute(&tpl, data); err != nil {
		return err
	}
	result.Txt = tpl.String()
	return nil
}

// email template
type MailTpl interface {
	GetContent(key dao.MailTplKey, data interface{}) (result *MailTplResult, err error)
	GetTemplate(key dao.MailTplKey) (result *MailTplResult, err error)
	SetTemplate(key dao.MailTplKey, in *dao.MailTemplate, u coreDao.LogUser) error
}

func NewMongoMail(ctx context.Context, db *mongo.Database, l log.Logger) MailTpl {
	return &mongoMail{
		mgo: mgom.NewMgoModel(ctx, db, l),
		log: l,
	}
}

type mongoMail struct {
	mgo mgom.MgoDBModel
	db  *mongo.Database
	log log.Logger
}

func (mail *mongoMail) GetContent(key dao.MailTplKey, data interface{}) (result *MailTplResult, err error) {
	result, err = mail.GetTemplate(key)

	if err != nil {
		return
	}
	err = result.render(data)
	return
}

func (mail *mongoMail) GetTemplate(key dao.MailTplKey) (result *MailTplResult, err error) {
	tpl := mongoDao.NewMailTplSetting(string(key))
	err = mail.mgo.FindByID(tpl)
	if err != nil {
		return
	}
	result = &MailTplResult{
		Title: tpl.Title,
		Html:  tpl.Html,
		Txt:   tpl.Txt,
	}
	return
}

func (mail *mongoMail) SetTemplate(key dao.MailTplKey, in *dao.MailTemplate, u coreDao.LogUser) error {
	if in == nil {
		return errors.New("input is null")
	}

	tpl := mongoDao.NewMailTplSetting(string(key))
	err := mail.mgo.FindByID(tpl)
	found := true
	if err != nil {
		found = false
		tpl.MailTemplate = &dao.MailTemplate{}
	}
	tpl.Title = html.EscapeString(in.Title)
	tpl.Txt = html.EscapeString(in.Txt)
	tpl.Html, err = util.RemoveScriptTag(in.Html)
	if err != nil {
		return err
	}

	if found {
		_, err = mail.mgo.UpdateOne(tpl, bson.D{
			{Key: "html", Value: tpl.Html},
			{Key: "txt", Value: tpl.Txt},
			{Key: "title", Value: tpl.Title},
		}, u)
	} else {
		_, err = mail.mgo.Save(tpl, u)
	}
	return err
}

// 暫留做gcp storage sample code
// type StoreObject struct {
// 	Bucket   string
// 	StoreKey string
// }

// func (so *StoreObject) GetKey(s dao.Format) string {
// 	switch s {
// 	case dao.FormatHTML:
// 		return fmt.Sprintf(htmlKeyPath, so.Bucket, so.StoreKey)
// 	case dao.FormatTXT:
// 		return fmt.Sprintf(txtKeyPath, so.Bucket, so.StoreKey)
// 	}
// 	return ""
// }

// func NewGcpMail(ctx context.Context, sto gcp.Storage) Mail {
// 	return &gcpMail{
// 		ctx:     ctx,
// 		storage: sto,
// 	}
// }

// type gcpMail struct {
// 	ctx     context.Context
// 	storage gcp.Storage
// }

// func (mail *gcpMail) GetContent(s dao.Format, obj *StoreObject, data map[string]interface{}) (string, error) {
// 	key := obj.GetKey(s)
// 	tplstr, err := mail.GetTemplate(s, obj)
// 	if err != nil {
// 		return "", err
// 	}
// 	t, err := template.New(key).Parse(tplstr)
// 	if err != nil {
// 		return "", err
// 	}
// 	var tpl bytes.Buffer
// 	if err := t.Execute(&tpl, data); err != nil {
// 		return "", err
// 	}
// 	if s == dao.FormatTXT {
// 		return html.EscapeString(tpl.String()), nil
// 	}
// 	return tpl.String(), nil
// }

// func (mail *gcpMail) GetTemplate(s dao.Format, obj *StoreObject) (string, error) {
// 	key := obj.GetKey(s)
// 	read, err := mail.storage.OpenFile(mail.ctx, key)
// 	if read == nil {
// 		// 讀不到
// 		if obj.Bucket != dao.DefaultBucket {
// 			obj.Bucket = dao.DefaultBucket
// 			return mail.GetTemplate(s, obj)
// 		}
// 		return "", err
// 	}
// 	buf := new(strings.Builder)
// 	_, err = io.Copy(buf, read)
// 	if err != nil {
// 		return "", err
// 	}
// 	return buf.String(), nil
// }

// func (mail *gcpMail) SetTemplate(s dao.Format, obj *StoreObject, tpl string) error {
// 	key := obj.GetKey(s)

// 	if s == dao.FormatHTML {
// 		doc, err := html.Parse(strings.NewReader(tpl))
// 		if err != nil {
// 			return err
// 		}
// 		removeScript(doc)
// 		buf := bytes.NewBuffer([]byte{})
// 		if err := html.Render(buf, doc); err != nil {
// 			return err
// 		}
// 		return mail.storage.WriteString(mail.ctx, key, buf.String())
// 	}
// 	return mail.storage.WriteString(mail.ctx, key, tpl)
// }
