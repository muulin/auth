package model

import (
	"net/http"

	dao "auth/dao"
	mon "auth/dao/mongo"

	apiErr "github.com/94peter/sterna/api/err"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/model/mgom"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserMeta interface {
	FindByID(id interface{}) (dao.UserMeta, error)
}

func NewMongoUserMeta(mgom mgom.MgoDBModel) UserMeta {
	return &mongoUserMeta{
		MgoDBModel: mgom,
	}
}

type mongoUserMeta struct {
	mgom.MgoDBModel
}

func (impl *mongoUserMeta) FindByID(id interface{}) (dao.UserMeta, error) {
	oid, err := sternaDao.GetObjectID(id)
	if err != nil {
		return nil, err
	}
	result := &mon.UserMeta{
		ID: oid,
	}
	err = impl.MgoDBModel.FindByID(result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, apiErr.New(http.StatusNotFound, "user meta not found")
		}
		return nil, err
	}
	return result.UserMeta, nil
}
