package model

import (
	"context"
	"net/http"
	"time"

	dao "auth/dao"

	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	OtpPrefix = "AuthOtp_"
)

type otpModel struct {
	mgoModel mgom.MgoDBModel
	log      log.Logger
	redis    db.RedisClient
}

func NewOtpModel(ctx context.Context, db *mongo.Database, l log.Logger, r db.RedisClient) OtpInterface {
	return &otpModel{
		mgoModel: mgom.NewMgoModel(ctx, db, l),
		log:      l,
		redis:    r,
	}
}

type OtpInterface interface {
	OtpLogin(domain, account, code, platform string) (*dao.LoginUser, error)
	OtpChangePwd(auth.ReqUser, *dao.OtpChangePwd) error
	ForgetPwd(in *dao.ForgetPwd) (resp *ForgetPwdResp, err error)
}

type ForgetPwdResp struct {
	User *dao.User
	Code string
}

func (impl *otpModel) ForgetPwd(in *dao.ForgetPwd) (resp *ForgetPwdResp, err error) {
	crud := NewCrudByModel(impl.mgoModel, impl.log)
	u, err := crud.FindUser(in.Email)
	if err != nil {
		return
	}

	totp := auth.NewTotp(in.Host, u.Email, u.Pwd, OtpPeriodSecs)
	code, err := totp.GenerateCode()
	if err != nil {
		return
	}

	_, err = impl.redis.Set(OtpPrefix+code, in.Email, OtpPeriodSecs*time.Second)
	if err != nil {
		return
	}
	resp = &ForgetPwdResp{
		User: u.User,
		Code: code,
	}
	return
}

func (impl *otpModel) OtpLogin(domain, account, code, platform string) (*dao.LoginUser, error) {

	res, err := impl.redis.Get(OtpPrefix + code)
	if err != nil {
		return nil, apiErr.New(http.StatusForbidden, "otp code not found")
	}
	if string(res) != account {
		return nil, apiErr.New(http.StatusForbidden, "email and otp not match")
	}

	crud := NewCrudByModel(impl.mgoModel, impl.log)
	docUser, err := crud.FindUser(account)
	if err != nil {
		return nil, err
	}

	roles := docUser.GetRoles(platform)
	if len(roles) == 0 {
		return nil, apiErr.New(http.StatusUnauthorized, "user has no role")
	}

	return &dao.LoginUser{
		ID:      docUser.ID.Hex(),
		Name:    docUser.Name,
		Account: docUser.Email,
		Platform: dao.ServicePerm{
			Platform: platform,
			Roles:    roles,
		},
		State: docUser.GetState(),
	}, nil
}

func (impl *otpModel) OtpChangePwd(reqUser auth.ReqUser, in *dao.OtpChangePwd) error {

	emailByte, err := impl.redis.Get(OtpPrefix + in.Otp)
	if err != nil {
		return apiErr.New(http.StatusForbidden, "invalid otp")
	}

	crud := NewCrudByModel(impl.mgoModel, impl.log)
	user, err := crud.FindUser(string(emailByte))
	if err != nil {
		return err
	}

	_, err = impl.mgoModel.UpdateOne(user, bson.D{
		{Key: "pwd", Value: util.MD5(in.NewPwd)},
		{Key: "lastchangepwdtime", Value: time.Now()},
	}, reqUser)
	if err != nil {
		return err
	}

	_, err = impl.redis.Del(in.Otp)
	if err != nil {
		return err
	}

	return nil
}
