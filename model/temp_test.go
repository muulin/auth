package model

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/94peter/sterna/auth"
	"github.com/stretchr/testify/assert"
)

func Test_Otp(t *testing.T) {
	otp := auth.NewTotp("host", "account", "password", 600)
	str, err := otp.GenerateCode()
	fmt.Println(str)
	if err != nil {
		fmt.Println(err.Error())
	}

	str, err = otp.GenerateCode()
	fmt.Println(str)
	if err != nil {
		fmt.Println(err.Error())
	}
	assert.False(t, true)
}

func Test_JsonDecode(t *testing.T) {
	jsonStr := `{"extra_config": {
		"telemetry/logging": {
		  "level": "DEBUG",
		  "prefix": "[KRAKEND]",
		  "syslog": false,
		  "stdout": true
		},
		"security/cors": {
		  "allow_origins": [ "*" ],
		  "allow_methods": [ "GET" ],
		  "allow_headers": [ "Origin", "Authorization", "Content-Type" ],
		  "expose_headers": [ "Content-Length" ],
		  "max_age": "12h"
		},
		"plugin/http-server": {
		  "name": ["static-filesystem"],
		  "static-filesystem": {
			"prefix": "/doc/*",
			"path": "./doc"
		  },
		  "name": ["no-supported"],
		  "no-supported": {
			"endpoints": [
			  { "path": "/v1/login", "method": "POST" }
			]
		  }
		}
	  }}`

	data := map[string]any{}
	json.Unmarshal([]byte(jsonStr), &data)
	conf, _ := data["extra_config"].(map[string]any)
	fmt.Println(conf)
	plugin, _ := conf["plugin/http-server"].(map[string]any)
	fmt.Println(plugin)
	noSupported, _ := plugin["no-supported"].(map[string]any)
	fmt.Println(noSupported)

	endpoints, _ := noSupported["endpoints"].([]any)
	fmt.Printf("aaaa, %v", endpoints)
	for _, i := range endpoints {
		fmt.Println(i)
	}
	assert.False(t, true)
}
