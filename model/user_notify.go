package model

import (
	"errors"
	"strings"

	dao "auth/dao"
	"auth/dao/mongo"

	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/model/mgom"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func NewUserNotify(mgom mgom.MgoDBModel) UserNotify {
	return &mongoUserNotify{
		MgoDBModel: mgom,
	}
}

type UserNotify interface {
	AddNotify(userid interface{}, phoneId string, platform dao.UserNotifyPlatform, token string) error
}

type mongoUserNotify struct {
	mgom.MgoDBModel
}

func (impl *mongoUserNotify) AddNotify(userid interface{}, phoneId string, platform dao.UserNotifyPlatform, token string) error {
	if !platform.IsValid() {
		return errors.New("invalid notify platform")
	}
	oid, err := sternaDao.GetObjectID(userid)
	if err != nil {
		return errors.New("invliad userid")
	}
	user := &mongo.UserNotify{
		ID: oid,
	}
	err = impl.MgoDBModel.FindByID(user)
	found := true
	if err != nil && !strings.Contains(err.Error(), "no documents in result") {
		return err
	} else if err != nil {
		found = false
	}

	if user.UserNotify == nil {
		user.UserNotify = make(dao.UserNotify)
	}
	err = user.AddNotify(phoneId, platform, token)
	if err != nil {
		return err
	}

	if !found {
		_, err = impl.MgoDBModel.Save(user, nil)
		return err
	}

	_, err = impl.MgoDBModel.UpdateOne(user, primitive.D{
		{Key: "usernotify", Value: user.UserNotify},
	}, nil)
	return err

}
