package model

import (
	"net/http"
	"time"

	dao "auth/dao"
	mon "auth/dao/mongo"

	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/94peter/sterna/util"
	"go.mongodb.org/mongo-driver/bson"
)

type UserPwdLogin interface {
	Login(domain, user, pass, platform string) (*dao.LoginUser, error)
	ChangePwd(user auth.ReqUser, in *dao.ChangePwd) error
}

func NewUserPwdLogin(mgom mgom.MgoDBModel, l log.Logger) UserPwdLogin {
	return &mgoUserPwdLogin{
		MgoDBModel: mgom,
		log:        l,
	}
}

type mgoUserPwdLogin struct {
	mgom.MgoDBModel
	log log.Logger
}

func (impl *mgoUserPwdLogin) Login(domain, user, pass, platform string) (*dao.LoginUser, error) {
	u := &mon.User{}
	err := impl.FindOne(u, bson.M{
		"email": user,
		"pwd":   util.MD5(pass),
	})
	if err != nil {
		return nil, apiErr.New(http.StatusNotFound, err.Error())
	}

	roles := u.GetRoles(platform)
	if len(roles) == 0 {
		return nil, apiErr.New(http.StatusUnauthorized, "user has no role")
	}

	return &dao.LoginUser{
		ID:      u.ID.Hex(),
		Domain:  domain,
		Name:    u.Name,
		Account: u.Email,
		Platform: dao.ServicePerm{
			Platform: platform,
			Roles:    roles,
		},
		State:    u.GetState(),
		Channels: u.Channels,
	}, nil
}

func (impl *mgoUserPwdLogin) ChangePwd(user auth.ReqUser, in *dao.ChangePwd) error {
	uid, err := sternaDao.GetObjectID(user.GetId())
	if err != nil {
		return err
	}

	crud := NewCrudByModel(impl.MgoDBModel, impl.log)
	u, err := crud.FindByID(uid)
	if err != nil {
		return err
	}
	if u.Pwd != util.MD5(in.OldPwd) {
		return apiErr.New(http.StatusBadRequest, "origin password error")
	}
	_, err = impl.UpdateOne(u, bson.D{
		{Key: "pwd", Value: util.MD5(in.NewPwd)},
		{Key: "lastchangepwdtime", Value: time.Now()},
	}, user)
	return err
}
