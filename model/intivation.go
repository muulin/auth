package model

import (
	"context"
	"net/http"
	"strings"
	"time"

	dao "auth/dao"
	mongoDao "auth/dao/mongo"
	input "auth/input"
	types "auth/types"

	apiErr "github.com/94peter/sterna/api/err"
	"github.com/94peter/sterna/auth"
	sternaDao "github.com/94peter/sterna/dao"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewIntivation(ctx context.Context, db *mongo.Database, l log.Logger) IntivationModel {
	return &invitation{
		dbmodel: mgom.NewMgoModel(ctx, db, l),
		log:     l,
	}
}

type IntivationModel interface {
	// check the inpput.Channel and input.Email be4 using this function
	Create(*input.CreateMail) (primitive.ObjectID, error)
	FindNewByEmail(email string) (*mongoDao.Intivation, error)
	FindByID(id interface{}) (*mongoDao.Intivation, error)
	GetByQuery(types.InvitationState, types.RegisterType) ([]*mongoDao.Intivation, error)
	Confirm(primitive.ObjectID, types.InvitationState) (*dao.Invitation, error)
	RemoveByID(primitive.ObjectID, auth.ReqUser) error
}

type invitation struct {
	dbmodel mgom.MgoDBModel
	log     log.Logger
}

func (r *invitation) FindNewByEmail(email string) (*mongoDao.Intivation, error) {
	query := bson.M{
		"email": email,
		"state": types.InvitationStateNew,
	}
	result := &mongoDao.Intivation{}
	err := r.dbmodel.FindOne(result, query)
	if err != nil {
		if strings.Contains(err.Error(), "no documents in result") {
			return nil, nil
		}
		return nil, err
	}

	return result, nil
}

func (r *invitation) Confirm(id primitive.ObjectID, state types.InvitationState) (*dao.Invitation, error) {
	d, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}

	if d.State != types.InvitationStateNew {
		return nil, apiErr.New(http.StatusForbidden, "current invitation's state cannot be change")
	}

	d.AddLog(time.Now(), string(state))
	_, err = r.dbmodel.UpdateOne(d, bson.D{
		{Key: "logs", Value: d.Logs},
		{Key: "state", Value: state},
	}, nil)
	if err != nil {
		return nil, err
	}

	return d.Invitation, nil
}

func (r *invitation) RemoveByID(id primitive.ObjectID, u auth.ReqUser) error {
	invitation, err := r.FindByID(id)
	if err != nil {
		return err
	}

	if invitation.State != types.InvitationStateNew {
		return apiErr.New(http.StatusForbidden, "invitation that been confirm cannot be remove")
	}

	_, err = r.dbmodel.RemoveByID(&mongoDao.Intivation{ID: id}, u)
	if err != nil {
		return err
	}

	return nil
}

func (r *invitation) GetByQuery(state types.InvitationState, typ types.RegisterType) ([]*mongoDao.Intivation, error) {
	query := bson.M{}

	if state != "" {
		query["state"] = state
	}

	if typ != "" {
		query["type"] = typ
	}

	res, err := r.dbmodel.Find(&mongoDao.Intivation{}, query)
	if err != nil {
		return nil, err
	}

	return res.([]*mongoDao.Intivation), nil
}

func (r *invitation) Create(in *input.CreateMail) (primitive.ObjectID, error) {
	inv := in.ToNewDao()
	d := &mongoDao.Intivation{
		Invitation: inv,
		// Template: &msgPlugin.MailMessage{
		// 	Message: &msgPlugin.Message{
		// 		MsgType: msgPlugin.Mail,
		// 		Receivers: []*msgPlugin.Receiver{
		// 			{
		// 				Name:    in.Name,
		// 				Address: in.Email,
		// 			},
		// 		},
		// 	},
		// 	Title: types.InvitationTittle,
		// 	Content: struct {
		// 		Plaint string
		// 		Html   string
		// 	}{
		// 		Plaint: types.InvitationPlaintTplKey,
		// 		Html:   types.InvitationHtmlTplKey,
		// 	},
		// 	Variables: types.InvitationVar,
		// },
	}

	id, err := r.dbmodel.Save(d, nil)
	if err != nil {
		if mongo.IsDuplicateKeyError(err) {
			return primitive.NilObjectID, apiErr.New(http.StatusConflict, err.Error())
		}
		return primitive.NilObjectID, err
	}

	return id.(primitive.ObjectID), nil
}

func (r *invitation) FindByID(id interface{}) (*mongoDao.Intivation, error) {
	oid, err := sternaDao.GetObjectID(id)
	if err != nil {
		return nil, apiErr.New(http.StatusBadRequest, err.Error()+"(invalid id)")
	}
	in := &mongoDao.Intivation{
		ID: oid,
	}
	err = r.dbmodel.FindByID(in)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, apiErr.New(http.StatusNotFound, "invitation not found")
		}
		return nil, err
	}
	return in, nil
}
