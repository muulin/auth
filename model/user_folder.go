package model

import (
	"strings"

	"auth/dao"
	"auth/dao/mongo"
	"auth/input"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/model/mgom"
)

type UserFolder interface {
	Upser(in input.CreateUserProjectFolders, reqUser auth.ReqUser) error
	Get(id string) (dao.Folders, error)
}

func NewUserFolder(mgom mgom.MgoDBModel) UserFolder {
	return &mongoUserFolder{
		MgoDBModel: mgom,
	}
}

type mongoUserFolder struct {
	mgom.MgoDBModel
}

func (m *mongoUserFolder) Upser(in input.CreateUserProjectFolders, reqUser auth.ReqUser) error {
	oid, err := primitive.ObjectIDFromHex(reqUser.GetId())
	if err != nil {
		return err
	}
	folders := &mongo.Folder{
		ID: oid,
	}
	for _, v := range in {
		folders.Folders = append(folders.Folders, dao.NewUserProjectFolder(
			v.Name, v.ProjectList,
		))
	}
	_, err = m.MgoDBModel.Upsert(folders, reqUser)
	return err
}

func (m *mongoUserFolder) Get(id string) (dao.Folders, error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}
	folders := &mongo.Folder{
		ID: oid,
	}
	err = m.MgoDBModel.FindByID(folders)
	if err != nil && !strings.Contains(err.Error(), "no documents in result") {
		return nil, err
	}
	return folders.Folders, nil
}
