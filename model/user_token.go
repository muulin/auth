package model

import (
	"errors"

	"auth/dao"

	"github.com/94peter/sterna/api/mid"
	"github.com/94peter/sterna/auth"
	"github.com/dgrijalva/jwt-go"
)

type TokenParserResult interface {
	mid.TokenParserResult
	Channels() []string
}

type UserToken interface {
	GetAuthTokenParser() mid.AuthTokenParser
	Parser(token string) (TokenParserResult, error)
	Generate(user *dao.LoginUser) (*string, error)
}

type myTokenParserResult struct {
	mid.TokenParserResult
	channels []string
}

func (my *myTokenParserResult) Channels() []string {
	return my.channels
}

func parserToken(jt auth.JwtToken, token string) (TokenParserResult, error) {
	t, err := jt.ParseToken(token)
	if err != nil {
		return nil, err
	}
	mapClaims := t.Claims.(jwt.MapClaims)
	iss, ok := mapClaims["iss"].(string)
	if !ok {
		return nil, errors.New("iss error")
	}
	acc, ok := mapClaims["acc"].(string)
	if !ok {
		return nil, errors.New("acc error")
	}
	sub, ok := mapClaims["sub"].(string)
	if !ok {
		return nil, errors.New("sub error")
	}
	name, ok := mapClaims["nam"].(string)
	if !ok {
		return nil, errors.New("name error")
	}
	channels, ok := mapClaims["cha"].([]any)
	if !ok {
		return nil, errors.New("channels error")
	}
	cs := []string{}
	for _, i := range channels {
		cs = append(cs, i.(string))
	}

	perm, ok := mapClaims["per"].([]interface{})
	if !ok {
		return &myTokenParserResult{
			TokenParserResult: NewTokenResult(iss, acc, sub, name, "", []string{string(auth.PermGuest)}),
			channels:          cs,
		}, nil
	}
	perms := []string{}
	for _, i := range perm {
		perms = append(perms, i.(string))
	}
	if len(perms) == 0 {
		perms = []string{string(auth.PermGuest)}
	}
	return &myTokenParserResult{
		TokenParserResult: NewTokenResult(iss, acc, sub, name, "", perms),
		channels:          cs,
	}, nil
}

func NewJwtUserToken(token auth.JwtToken) UserToken {
	return &jwtUserToken{
		JwtToken: token,
	}
}

type jwtUserToken struct {
	auth.JwtToken
}

func (j *jwtUserToken) Generate(user *dao.LoginUser) (*string, error) {
	return j.GetToken(user.Domain, map[string]interface{}{
		"sub": user.ID,
		"acc": user.Account,
		"nam": user.Name,
		"per": user.Platform.Roles,
		"cha": user.Channels,
	}, 180)
}

func (j *jwtUserToken) Parser(token string) (TokenParserResult, error) {
	return parserToken(j, token)
}

func (j *jwtUserToken) parser(token string) (mid.TokenParserResult, error) {
	return parserToken(j, token)
}

func (j *jwtUserToken) GetAuthTokenParser() mid.AuthTokenParser {
	return j.parser
}

func NewTokenResult(host, account, sub, name, target string, perms []string) mid.TokenParserResult {
	return &tokenResult{
		host:    host,
		target:  target,
		account: account,
		sub:     sub,
		name:    name,
		perm:    perms,
	}
}

type tokenResult struct {
	host    string
	target  string
	perm    []string
	account string
	sub     string
	name    string
}

func (t *tokenResult) Host() string {
	return t.host
}

func (t *tokenResult) Perms() []string {
	return t.perm
}

func (t *tokenResult) Account() string {
	return t.account
}

func (t *tokenResult) Name() string {
	return t.name
}

func (t *tokenResult) Sub() string {
	return t.sub
}

func (t *tokenResult) Target() string {
	return t.target
}
