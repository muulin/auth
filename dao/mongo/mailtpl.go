package mongo

import (
	dao "auth/dao"

	coreDao "github.com/94peter/sterna/dao"

	"go.mongodb.org/mongo-driver/mongo"
)

const (
	settingC = "setting"
)

func NewMailTplSetting(id string) *mailTplSetting {
	tplSetting := &dao.MailTemplate{}
	return &mailTplSetting{
		ID:           "mailTpl_" + id,
		MailTemplate: tplSetting,
	}
}

type mailTplSetting struct {
	ID                string `bson:"_id"`
	*dao.MailTemplate `bson:",inline"`
	coreDao.CommonDoc `bson:",inline"`
}

func (u *mailTplSetting) GetC() string {
	return settingC
}

func (u *mailTplSetting) GetID() interface{} {
	return u.ID
}

func (u *mailTplSetting) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{}
}

func (u *mailTplSetting) GetDoc() interface{} {
	return u
}
