package mongo

import (
	"auth/dao"

	sternaDao "github.com/94peter/sterna/dao"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const folderC = "folder"

type Folder struct {
	ID primitive.ObjectID `bson:"_id"`
	dao.Folders
	sternaDao.CommonDoc `bson:",inline"`
}

func (d *Folder) GetC() string {
	return folderC
}

func (d *Folder) GetID() interface{} {
	return d.ID
}

func (d *Folder) GetDoc() interface{} {
	return d
}

func (d *Folder) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{}
}
