package mongo

import (
	dao "auth/dao"
	types "auth/types"

	sternaDao "github.com/94peter/sterna/dao"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const sevenDaysSec = 604800

type Intivation struct {
	ID              primitive.ObjectID `bson:"_id"`
	*dao.Invitation `bson:",inline"`
	// Template        *msgPlugin.MailMessage
}

func (u *Intivation) GetC() string {
	return "invitation"
}

func (u *Intivation) GetID() interface{} {
	return u.ID
}

func (g *Intivation) GetDoc() interface{} {
	if g.ID.IsZero() {
		g.ID = primitive.NewObjectID()
	}
	return g
}

func (c *Intivation) AddRecord(u sternaDao.LogUser, msg string) []*sternaDao.Record {
	return nil
}

func (c *Intivation) SetCreator(lu sternaDao.LogUser) {
}

func (u *Intivation) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "email", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.D{
				{Key: "createdat", Value: 1},
			},
			Options: options.Index().
				SetExpireAfterSeconds(sevenDaysSec).
				SetPartialFilterExpression(bson.D{
					{Key: "state", Value: types.InvitationStateNew},
				}),
		},
	}
}
