package mongo

import (
	dao "auth/dao"

	sternaDao "github.com/94peter/sterna/dao"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	ID                  primitive.ObjectID `bson:"_id"`
	*dao.User           `bson:",inline"`
	ParentUserId        *primitive.ObjectID
	sternaDao.CommonDoc `bson:",inline"`
}

func (u *User) GetC() string {
	return "user"
}

func (u *User) GetID() interface{} {
	return u.ID
}

func (g *User) GetDoc() interface{} {
	if g.ID.IsZero() {
		g.ID = primitive.NewObjectID()
	}
	return g
}

func (u *User) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "email", Value: 1},
				{Key: "pwd", Value: 1},
			},
		},
		{
			Keys: bson.D{
				{Key: "email", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
	}
}

type UserMeta struct {
	ID           primitive.ObjectID `bson:"_id"`
	dao.UserMeta `bson:",inline"`

	sternaDao.CommonDoc `bson:",inline"`
}

func (u *UserMeta) GetID() interface{} {
	return u.ID
}

func (g *UserMeta) GetDoc() interface{} {
	return g
}

func (u *UserMeta) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{}
}

func (u *UserMeta) GetC() string {
	return "userMeta"
}

type UserNotify struct {
	ID primitive.ObjectID `bson:"_id"`
	dao.UserNotify
	sternaDao.CommonDoc `bson:",inline"`
}

func (u *UserNotify) GetID() interface{} {
	return u.ID
}

func (g *UserNotify) GetDoc() interface{} {
	return g
}

func (u *UserNotify) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{}
}

func (u *UserNotify) GetC() string {
	return "userNotify"
}
