package dao

type Platform struct {
	Key  string
	Name string
}

type Platforms []*Platform
