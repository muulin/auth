package dao

import (
	"time"

	types "auth/types"

	"github.com/94peter/sterna/auth"
)

type Invitation struct {
	Name      string
	Email     string
	Channel   string
	State     types.InvitationState
	Type      types.RegisterType
	Roles     []auth.UserPerm
	CreatedAt time.Time
	Logs      []*Log // DISCUSS where to add log? (except create and confirm)
}

func (i *Invitation) AddLog(t time.Time, msg string) {
	i.Logs = append(i.Logs, &Log{Time: t, Msg: msg})
}

type Log struct {
	Time time.Time
	Msg  string
}
