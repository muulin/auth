package dao

type Format string
type MailTplKey string

func (tpl MailTplKey) Valid() bool {
	return (tpl == MailTplRegister ||
		tpl == MailTplForget)
}

const (
	FormatHTML = Format("html")
	FormatTXT  = Format("txt")

	MailTplRegister   = MailTplKey("register")
	MailTplForget     = MailTplKey("forget")
	MailTplInvitation = MailTplKey("invitation")
)

type MailTemplate struct {
	Title     string
	Html      string
	Txt       string
	Parmaters []*Paramater
}

type Paramater struct {
	Display string
	Var     string
}
