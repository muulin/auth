package dao

import (
	"errors"
	"time"

	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/util"
)

const (
	emptyStr = ""
)

type LoginUser struct {
	ID       string
	Domain   string
	Name     string
	Account  string
	Platform ServicePerm
	Channels []string
	State    UserState
}

type userType string

const (
	// 主帳號
	MainUser = userType("main")
	// 子帳號
	SubUser = userType("sub")
)

type User struct {
	Name              string
	Type              userType
	Email             string
	Pwd               string
	Enable            bool
	IsEmailVerified   bool
	LastChangePwdTime time.Time
	UserServPerm      []*ServicePerm
	Channels          []string
	Lang              string
}

func (u *User) GetName() string {
	return u.Name
}

func (u *User) GetAccount() string {
	return u.Email
}

func (u *User) GetState() UserState {
	if !u.IsEmailVerified {
		return UserStateVerifying
	}
	if !u.Enable {
		return UserStateDisabled
	}
	return UserStateNormal
}

func (u *User) GetRoles(platform string) []auth.UserPerm {
	var roles []auth.UserPerm
	for _, p := range u.UserServPerm {
		if p.Platform == platform {
			roles = p.Roles
			break
		}
	}
	return roles
}

func (u *User) AddPlatform(platform string, roles []auth.UserPerm) {
	found := false

	for i, p := range u.UserServPerm {
		if p.Platform == platform {
			found = true
			u.UserServPerm[i].Roles = roles
		}
	}
	if found {
		return
	}
	u.UserServPerm = append(u.UserServPerm, &ServicePerm{
		Platform: platform,
		Roles:    roles,
	})
}

type ServicePerm struct {
	Platform string
	Roles    []auth.UserPerm
}

type UserMeta map[string]interface{}

type UserNotifyPlatform string

func (nt UserNotifyPlatform) IsValid() bool {
	return (nt == UserNotifyPlatformiOS || nt == UserNotifyPlatformAndroid || nt == UserNotifyPlatformMqtt)

}

const (
	UserNotifyPlatformiOS     = UserNotifyPlatform("ios")
	UserNotifyPlatformAndroid = UserNotifyPlatform("android")
	UserNotifyPlatformMqtt    = UserNotifyPlatform("mqtt")
)

type UserNotify map[string]struct {
	Platform  UserNotifyPlatform
	PushToken string
}

func (un UserNotify) AddNotify(phoneID string, platform UserNotifyPlatform, pushToken string) error {
	if un == nil {
		return errors.New("notify map is nil")
	}
	notify, isFound := un[phoneID]
	if !isFound {
		un[phoneID] = struct {
			Platform  UserNotifyPlatform
			PushToken string
		}{
			Platform:  platform,
			PushToken: pushToken,
		}
	} else {
		notify.Platform = platform
		notify.PushToken = pushToken
		un[phoneID] = notify
	}
	return nil
}

type UserState string

const (
	UserStateNormal    = UserState("normal")
	UserStateDisabled  = UserState("disable")
	UserStateVerifying = UserState("verifying")
)

type VerifyEmail struct {
	Email string
	Host  string
	Code  string
}

func (qb *VerifyEmail) Validate() error {
	if qb.Email == "" {
		return errors.New("miss email")
	}
	if qb.Host == "" {
		return errors.New("miss host")
	}
	if qb.Code == "" {
		return errors.New("miss code")
	}
	return nil
}

type Register struct {
	InvitationID string `json:"invitationId"`
	Name         string
	Pwd          string `json:"password"`
	Lang         string `json:"langKey"`
	Meta         UserMeta
}

type OtpChangePwd struct {
	Otp    string
	NewPwd string
}

func (c OtpChangePwd) Validate() error {
	if c.Otp == emptyStr {
		return errors.New("empty otp")
	}
	if c.NewPwd == emptyStr {
		return errors.New("empty newpwd")
	}
	return nil
}

type ChangePwd struct {
	OldPwd string
	NewPwd string
}

func (c *ChangePwd) Validate() error {
	if c.OldPwd == emptyStr {
		return errors.New("missing old")
	}
	if c.NewPwd == emptyStr {
		return errors.New("missing pwd")
	}
	return nil
}

type ForgetPwd struct {
	Host  string
	Email string
}

func (c *ForgetPwd) Validate() error {
	if c.Host == emptyStr {
		return errors.New("missing host")
	}
	if c.Email == emptyStr {
		return errors.New("missing email")
	}
	if ok, _ := util.IsMail(c.Email); !ok {
		return errors.New("invalid email format")
	}
	return nil
}

type OtpLogin struct {
	Host  string
	Email string
	Code  string
}

func (c *OtpLogin) Validate() error {
	if c.Host == emptyStr {
		return errors.New("missing host")
	}
	if c.Email == emptyStr {
		return errors.New("missing email")
	}
	isMail, _ := util.IsMail(c.Email)
	if !isMail {
		return errors.New("invalid email format")
	}
	if c.Code == emptyStr {
		return errors.New("missing code")
	}
	return nil
}
