package dao

import (
	"encoding/json"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
)

type FolderType string

const (
	FolderType_UserProject = FolderType("user_project")
)

type Folder interface {
	Type() FolderType
	GetName() string
	GetContent() []string
}

type Folders []*PolyFolder

func (l *Folders) AddFolder(f Folder) {
	if l == nil {
		return
	}
	*l = append(*l, &PolyFolder{Folder: f})
}

type PolyFolder struct {
	Folder
}

func (f *PolyFolder) Unmarshal(b []byte, myfunc func(data []byte, v interface{}) error) error {
	var v CommonFolder
	err := myfunc(b, &v)
	if err != nil {
		return err
	}
	var i Folder
	switch v.Type() {
	case FolderType_UserProject:
		i = &userProjectFolder{
			CommonFolder: &v,
		}
	default:
		return errors.New("unknown donate type")
	}
	err = myfunc(b, i)
	if err != nil {
		return err
	}
	f.Folder = i
	return nil
}

func (f *PolyFolder) UnmarshalBSON(b []byte) error {
	return f.Unmarshal(b, bson.Unmarshal)
}

func (f *PolyFolder) UnmarshalJSON(b []byte) error {
	return f.Unmarshal(b, json.Unmarshal)
}

func (f *PolyFolder) MarshalJSON() ([]byte, error) {
	return json.Marshal(f.Folder)
}

func (f *PolyFolder) MarshalBSON() ([]byte, error) {
	return bson.Marshal(f.Folder)
}

type CommonFolder struct {
	Typ  FolderType `json:"type" bson:"type"`
	Name string     `json:"name"`
}

func (d *CommonFolder) Type() FolderType {
	return d.Typ
}

func (d *CommonFolder) GetName() string {
	return d.Name
}

func NewUserProjectFolder(name string, projects []string) *PolyFolder {
	return &PolyFolder{
		Folder: &userProjectFolder{
			CommonFolder: &CommonFolder{
				Typ:  FolderType_UserProject,
				Name: name,
			},
			Projects: projects,
		},
	}
}

type userProjectFolder struct {
	*CommonFolder `json:",inline" bson:",inline"`
	Projects      []string `json:"projects"`
}

func (pf *userProjectFolder) GetContent() []string {
	return pf.Projects
}
