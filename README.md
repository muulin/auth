# Auth Service


This project is Auth Service designed to mananger user authentication

## Prerequirement

- MongoDB

## 開發環境安裝


1. [install golang](https://go.dev/doc/install)

	golang 版本建議為：`1.18` 以上

2. [install Docker](https://docs.docker.com/engine/install/)

3. [develop infrastructure deploy]()
```bash
# 啟動mongo container
git clone
cd mongo
sh start.sh

cd redis
docker-compose up -d
```

### How to get source code

```bash
git clone git@bitbucket.org:muulin/auth.git
```

### Develop config file sample
```yaml
mongo:
    uri: mongodb://mongo2:27018,mongo3:27019,mongo1:27017/?replicaSet=myReplicaSet&readPreference=primary
    defaul: dev-ml-v5-sys

redis:
    host: 127.0.0.1:6379
    pass: ""
    dbMap:
      auth: 0

log:
    level: debug
    target: os

jwtConf:
    privatekey: "/Users/peter/goproject/ml/auth/config/etc/dev/privatekey.pem"
    publickey: "/Users/peter/goproject/ml/auth/config/etc/dev/publickey.pem"
    header:
      alg: RS256
      typ: JWT
      kid: ML-SSO
    claims:
      exp: 1h

platforms:
    - name: Admin
      key: admin
    - name: Collaboration
      key: coop
    - name: Universal-App
      key: univ-app
    - name: Universal-Desk
      key: univ-desk
```



## Environment Variable

```env
# assign config path
environment=dev|beta|prod

# assign api service port
API_PORT=9086

# assign grpc service port
GRPC_PORT=7080

# allow Cross-Origin Resource Sharing        
API_CORS=true|false 

# select api mode
GIN_MODE=debug|test|release

# select api permission mode
API_AUTH_MODE=release|mock

# name for service as log prefix
SERVICE=auth

# set config file root path
CONF_PATH=./config/

# secret code for admin to create user
Auth_Code=42697846

# trusted proxies
TRUSTED_PROXIES=127.0.0.1

# log level
LOG_LEVEL=debug|info|warn|error|fatal

# log message send to target
LOG_TARGET=os|fluent
```


### Run project

```bash
make run
```
or
```bash
go run .
```

## 資料夾說明

- api - api handler
- grpc - grpc service & protobuf
- dao - data asscess object
- input - user input model
- model - functional logic
- doc - documentation


## 專案技術

- [gin web framework](https://github.com/gin-gonic/gin)
- [grpcurl](https://github.com/fullstorydev/grpcurl)




## CI/CD 說明

> 待撰寫

此專案有使用 Github Actions，所以發起 PR 時會自動執行以下動作：

- 建立 Node.js 環境
- 安裝相依套件
- 編譯程式碼
- 執行 ESLint 掃描
- 執行測試
...

當專案 merge 到 main 時會自動執行以下動作：

- 建立 Node.js 環境
- 安裝相依套件
- 編譯程式碼
- 執行 ESLint 掃描
- 執行測試
- 部署到 Github Pages
...

