package types

const (
	ForgetPwdTittle = "[沐霖雲]忘記密碼"
	ForgetPwdPlaintTplKey = "ForgetPwdPT"
	ForgetPwdHtmlTplKey = "ForgetPwdHL"

	InvitationTittle = "[沐霖雲]使用邀請信"
	InvitationPlaintTplKey = "AuthInvPT"
	InvitationHtmlTplKey = "AuthInvHL"
)

var ForgetPwdVar = map[string]string{
	"tempForgetPwdKey": "tempForgetPwdValue",
}
var InvitationVar = map[string]string{
	"tempInvitationKey": "tempInvitationValue",
}