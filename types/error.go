package types

import (
	"fmt"
	"net/http"

	apiErr "github.com/94peter/sterna/api/err"
)

var (
	ErrTokenTimeout   = apiErr.NewWithKey(http.StatusUnauthorized, "token timeout", "10000")
	ErrMissingToken   = apiErr.NewWithKey(http.StatusUnauthorized, "missing", "10001")
	ErrInvalidToken   = apiErr.NewWithKey(http.StatusUnauthorized, "invalid token", "10002")
	ErrJsonEncodeFail = apiErr.NewWithKey(http.StatusUnauthorized, "json encdoe fail: %s", "10003")
)

func NewErrorWaper(err apiErr.ApiError, detail string) apiErr.ApiError {
	return apiErr.NewWithKey(err.GetStatus(), fmt.Sprintf(err.GetErrorMsg(), detail), err.GetErrorKey())
}
