package types

import "github.com/94peter/sterna/util"

type RegisterType string

const (
	OperatorRegister = RegisterType("operator_register")
	UserRegister = RegisterType("user_register")
)

func (t RegisterType) IsValid() bool {
	if !util.IsStrInList(string(t), string(OperatorRegister), string(UserRegister)) {
		return false
	}	
	return true
}

type InvitationState string

const (
	InvitationStateNew = InvitationState("new")
	InvitationStateRegistered = InvitationState("registered")
	InvitationStateDeclined = InvitationState("declined")
)

func (t InvitationState) IsValid() bool {
	if !util.IsStrInList(string(t), string(InvitationStateNew), string(InvitationStateRegistered)) {
		return false
	}	
	return true
}